<?php

namespace BlueMedia\OnlinePayments;

use BlueMedia\OnlinePayments\Action\ITN;
use BlueMedia\OnlinePayments\Action\PaywayList;
use BlueMedia\OnlinePayments\Model\ItnIn;
use BlueMedia\OnlinePayments\Util\XMLParser;
use GuzzleHttp;
use RuntimeException;
use XMLWriter;

/**
 * Gateway.
 *
 * @author    Piotr Żuralski <piotr@zuralski.net>
 * @copyright 2015 Blue Media
 * @package   BlueMedia\OnlinePayments
 * @since     2015-08-08
 * @version   2.3.3
 */
class Gateway
{
    const MODE_SANDBOX = 'sandbox';
    const MODE_LIVE = 'live';

    const PAYMENT_DOMAIN_SANDBOX = 'pay-accept.bm.pl';
    const PAYMENT_DOMAIN_LIVE = 'pay.bm.pl';

    const PAYMENT_ACTON_PAYMENT     = '/payment';
    const PAYMENT_ACTON_PAYWAY_LIST = '/paywayList';
    const PAYMENT_ACTON_TEST_ECOMMERCE = '/test_ecommerce';

    const STATUS_CONFIRMED = 'CONFIRMED';
    const STATUS_NOT_CONFIRMED = 'NOTCONFIRMED';

    const DATETIME_FORMAT = 'YmdHis';
    const DATETIME_FORMAT_LONGER = 'Y-m-d H:i:s';
    const DATETIME_TIMEZONE = 'Europe/Warsaw';

    /** @var string */
    private $response = '';

    /** @var int */
    protected static $serviceId = 0;

    /** @var string */
    protected static $hashingSalt = '';

    /** @var string */
    protected static $mode = self::MODE_SANDBOX;

    /** @var string */
    protected static $hashingAlgorithm = '';

    /** @var string */
    protected static $hashingSeparator = '';

    /**
     * List of supported hashing algorithms.
     *
     * @var array
     */
    protected $hashingAlgorithmSupported = array(
        'md5' => 1,
        'sha1' => 1,
        'sha256' => 1,
        'sha512' => 1,
    );

    /**
     * Parse response from Payment System.
     *
     * @return Model\TransactionBackground|string
     */
    private function parseResponse()
    {
        $this->isErrorResponse();
        if ($this->isPaywayFormResponse()) {
            preg_match_all('@<!-- PAYWAY FORM BEGIN -->(.*)<!-- PAYWAY FORM END -->@Usi', $this->response, $data, PREG_PATTERN_ORDER);

            Logger::log(Logger::INFO, 'Got pay way form', array('data' => $data['1']['0'], 'full-response' => $this->response));

            return htmlspecialchars_decode($data['1']['0']);
        }

        return $this->parseTransferResponse();
    }

    /**
     * Parses transfer response.
     *
     * @return Model\TransactionBackground
     */
    private function parseTransferResponse()
    {
        $xmlData = XMLParser::parse($this->response);

        $transactionBackground = new Model\TransactionBackground();
        $transactionBackground
            ->setReceiverNrb((string) $xmlData->receiverNRB)
            ->setReceiverName((string) $xmlData->receiverName)
            ->setReceiverAddress((string) $xmlData->receiverAddress)
            ->setOrderId((string) $xmlData->orderID)
            ->setAmount((string) $xmlData->amount)
            ->setCurrency((string) $xmlData->currency)
            ->setTitle((string) $xmlData->title)
            ->setRemoteId((string) $xmlData->remoteID)
            ->setBankHref((string) $xmlData->bankHref)
            ->setHash((string) $xmlData->hash);

        $transactionBackgroundHash = self::generateHash($transactionBackground->toArray());
        if ($transactionBackgroundHash !== $transactionBackground->getHash()) {
            Logger::log(
                Logger::EMERGENCY,
                sprintf(
                    'Received wrong hash, calculated hash "%s", received hash "%s"',
                    $transactionBackgroundHash,
                    $transactionBackground->getHash()
                ),
                array('data' => $transactionBackground->toArray(), 'full-response' => $this->response)
            );
            throw new RuntimeException('Received wrong hash!');
        }

        return $transactionBackground;
    }

    /**
     * Is error response.
     *
     * @return void
     */
    private function isErrorResponse()
    {
        if (preg_match_all('@<error>(.*)</error>@Usi', $this->response, $data, PREG_PATTERN_ORDER)) {
            $xmlData = XMLParser::parse($this->response);
            Logger::log(
                Logger::EMERGENCY,
                sprintf('Got error: "%s", code: "%s"', $xmlData->name, $xmlData->statusCode),
                array('data' => $xmlData, 'full-response' => $this->response)
            );
            throw new RuntimeException((string) $xmlData->name);
        } elseif (preg_match_all('/error(.*)/si', $this->response, $data, PREG_PATTERN_ORDER)) {
            throw new RuntimeException($this->response);
        }
    }

    /**
     * Is pay way form response.
     *
     * @return int
     */
    private function isPaywayFormResponse()
    {
        return (preg_match_all('@<!-- PAYWAY FORM BEGIN -->(.*)<!-- PAYWAY FORM END -->@Usi', $this->response, $data, PREG_PATTERN_ORDER));
    }

    /**
     * Checks PHP required environment.
     *
     * @throws \RuntimeException
     *
     * @return void
     */
    protected function checkPhpEnvironment()
    {
        if (!version_compare(PHP_VERSION, '5.5', '>=')) {
            throw new RuntimeException(sprintf('Required at least PHP version 5.5, current version "%s"', PHP_VERSION));
        }
        if (!extension_loaded('xmlwriter')) {
            throw new RuntimeException('Extension "xmlwriter" is required');
        }
        if (!extension_loaded('xmlreader')) {
            throw new RuntimeException('Extension "xmlreader" is required');
        }
        if (!extension_loaded('iconv')) {
            throw new RuntimeException('Extension "iconv" is required');
        }
        if (!extension_loaded('mbstring')) {
            throw new RuntimeException('Extension "mbstring" is required');
        }
        if (!extension_loaded('hash')) {
            throw new RuntimeException('Extension "hash" is required');
        }
    }

    /**
     * Initialize.
     *
     * @api
     *
     * @param int    $serviceId
     * @param string $hashingSalt
     * @param string $mode
     * @param string $hashingAlgorithm
     * @param string $hashingSeparator
     *
     * @throws RuntimeException
     */
    public function __construct($serviceId, $hashingSalt, $mode = self::MODE_SANDBOX, $hashingAlgorithm = 'sha256', $hashingSeparator = '|')
    {
        $this->checkPhpEnvironment();

        if ($mode !== self::MODE_LIVE && $mode !== self::MODE_SANDBOX) {
            throw new RuntimeException(sprintf('Not supported mode "%s"', $mode));
        }
        if (!array_key_exists($hashingAlgorithm, $this->hashingAlgorithmSupported)) {
            throw new RuntimeException(sprintf('Not supported hashingAlgorithm "%s"', $hashingAlgorithm));
        }
        if (empty($serviceId) || !is_numeric($serviceId)) {
            throw new RuntimeException(sprintf('Not supported serviceId "%s" - must be integer, %s given', $serviceId, gettype($serviceId)));
        }
        if (empty($hashingSalt) || !is_string($hashingSalt)) {
            throw new RuntimeException(sprintf('Not supported hashingSalt "%s" - must be string, %s given', $hashingSalt, gettype($hashingSalt)));
        }

        self::$mode = $mode;
        self::$hashingAlgorithm = $hashingAlgorithm;
        self::$serviceId = $serviceId;
        self::$hashingSalt = $hashingSalt;
        self::$hashingSeparator = $hashingSeparator;
    }

    /**
     * Process ITN requests.
     *
     * @api
     *
     * @return Model\ItnIn|null
     */
    public function doItnIn()
    {
        if (empty($_POST['transactions'])) {
            Logger::log(
                Logger::INFO,
                sprintf('No "transactions" field in POST data'),
                array('_POST' => $_POST)
            );

            return;
        }

        $transactionXml = $_POST['transactions'];
        $transactionXml = base64_decode($transactionXml, true);
        $transactionData = XMLParser::parse($transactionXml);

        Logger::log(
            Logger::DEBUG,
            sprintf('Got "transactions" field in POST data'),
            array(
                'data-raw' => $_POST['transactions'],
                'data-xml' => $transactionXml,
            )
        );

        return ITN\Transformer::toModel($transactionData);
    }

    /**
     * Returns response for ITN IN request.
     *
     * @api
     *
     * @param ItnIn $transaction
     * @param bool  $transactionConfirmed
     *
     * @return string
     */
    public function doItnInResponse(Model\ItnIn $transaction, $transactionConfirmed = true)
    {
        $transactionHash = self::generateHash(ITN\Transformer::modelToArray($transaction));
        $confirmationStatus = self::STATUS_NOT_CONFIRMED;

        if ($transactionHash === $transaction->getHash()) {
            $confirmationStatus = self::STATUS_CONFIRMED;
        }
        if (!$transactionConfirmed) {
            $confirmationStatus = self::STATUS_NOT_CONFIRMED;
        }

        $confirmationList = array(
            'serviceID' => self::$serviceId,
            'orderID' => $transaction->getOrderId(),
            'confirmation' => $confirmationStatus,
        );

        $confirmationList['hash'] = self::generateHash($confirmationList);

        $xml = new XMLWriter();
        $xml->openMemory();
        $xml->startDocument('1.0', 'UTF-8');
        $xml->startElement('confirmationList');
        $xml->writeElement('serviceID', $confirmationList['serviceID']);
        $xml->startElement('transactionsConfirmations');
        $xml->startElement('transactionConfirmed');
        $xml->writeElement('orderID', $confirmationList['orderID']);
        $xml->writeElement('confirmation', $confirmationList['confirmation']);
        $xml->endElement();
        $xml->endElement();
        $xml->writeElement('hash', $confirmationList['hash']);
        $xml->endElement();

        return $xml->outputMemory();
    }

    /**
     * Perform transaction in background.
     *
     * @api
     *
     * @param Model\TransactionStandard $transaction
     *
     * @return Model\TransactionBackground|string
     */
    public function doTransactionBackground(Model\TransactionStandard $transaction)
    {
        $transaction->setServiceId(self::$serviceId);
        $transaction->setHash(self::generateHash($transaction->toArray()));
        $transaction->validate();

        $url = self::getActionUrl(self::PAYMENT_ACTON_PAYMENT);
        $request = new GuzzleHttp\Psr7\Request('POST', $url, array('BmHeader' => 'pay-bm'));

        $client = new GuzzleHttp\Client(array(
            GuzzleHttp\RequestOptions::VERIFY => true,
            'exceptions' => false,
        ));

        $responseObject = $client->send($request, array(GuzzleHttp\RequestOptions::FORM_PARAMS => $transaction->toArray()));
        $this->response = (string) $responseObject->getBody();

        return $this->parseResponse();
    }

    /**
     * Perform standard transaction.
     *
     * @api
     *
     * @param Model\TransactionStandard $transaction
     *
     * @return string
     */
    public function doTransactionStandard(Model\TransactionStandard $transaction)
    {
        $transaction->setServiceId(self::$serviceId);
        $transaction->setHash(self::generateHash($transaction->toArray()));
        $transaction->validate();

        return $transaction->getHtmlForm();
    }

    /**
     * Perform standard transaction.
     *
     * @api
     *
     * @param Model\TransactionStandard $transaction
     *
     * @return string
     */
    public function getStandardTransactionUrl(Model\TransactionStandard $transaction)
    {
        $transaction->setServiceId(self::$serviceId);
        $transaction->setHash(self::generateHash($transaction->toArray()));
        $transaction->validate();

        return $transaction->getStandardTransactionURL();
    }

//    /**
//     * Perform standard transaction.
//     *
//     * @api
//     *
//     * @param Model\PreTransaction $transaction
//     *
//     * @return string
//     */
//    public function doPreTransaction(Model\PreTransaction $transaction)
//    {
//        $data = array(
//            'ServiceID' => '100047',
//            'OrderID' => '20161017143213',
//            'Amount' => '1.00',
//            'Description' => 'test bramki',
//            'GatewayID' => '0',
//            'Currency' => 'PLN',
//            'CustomerEmail' => 'test@bramka.pl',
//            'CustomerIP' => '127.0.0.0',
//            'Title' => 'Test title',
//            'Hash' => '0c5ca136e8833e40efbf42a4da7c148c50bf99f8af26f5c9400681702bd72056'
//);
//
//        $fields = (is_array($data)) ? http_build_query($data) : $data;
//
//        $curl = curl_init('https://pay-accept.bm.pl/test_ecommerce');
//        curl_setopt($curl, CURLOPT_HTTPHEADER, array('BmHeader: pay-bm-continuetransaction-url'));
//        curl_setopt($curl, CURLOPT_POSTFIELDS, $fields);
//        curl_setopt($curl, CURLOPT_POST, 1);
//        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
//        $curlResponse = curl_exec($curl);
//        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
//        $response = curl_getinfo($curl);
//        curl_close($curl);
//
//        return htmlspecialchars_decode($curlResponse);
//
////        $transaction->setServiceId(self::$serviceId);
////        $transaction->setHash(self::generateHash($transaction->toArray()));
////        $transaction->validate();
////
////        $data = $transaction->toArray();
////
////        $request = new GuzzleHttp\Psr7\Request(
////            'POST',
////            self::getActionUrl(self::PAYMENT_ACTON_TEST_ECOMMERCE),
////            ['BmHeader'=> 'pay-bm-continuetransaction-url']
////        );
////
////        $client = new GuzzleHttp\Client(array(
////            GuzzleHttp\RequestOptions::VERIFY => true,
////            'exceptions' => false,
////        ));
////
////        $responseObject = $client->send($request, array(GuzzleHttp\RequestOptions::FORM_PARAMS => $data));
////
////        dd($responseObject->getBody()->getContents());
////        $this->response = (string) $responseObject->getBody();
////        $this->isErrorResponse();
////
////        $responseParsed = XMLParser::parse($this->response);
////
////        dump($responseParsed);
//    }



    /**
     * Maps payment mode to service payment domain.
     *
     * @api
     *
     * @param string $mode
     *
     * @return string
     */
    final public static function mapModeToDomain($mode)
    {
        switch ($mode) {
            case self::MODE_LIVE:
                return self::PAYMENT_DOMAIN_LIVE;

            default:
                return self::PAYMENT_DOMAIN_SANDBOX;
        }
    }

    /**
     * Maps payment mode to service payment URL.
     *
     * @api
     *
     * @param string $mode
     *
     * @return string
     */
    final public static function mapModeToUrl($mode)
    {
        $domain = self::mapModeToDomain($mode);

        return sprintf('https://%s', $domain);
    }

    /**
     * Returns payment service action URL.
     *
     * @api
     *
     * @param string $action
     *
     * @return string
     */
    final public static function getActionUrl($action)
    {
        $domain = self::mapModeToDomain(self::$mode);

        switch ($action) {
            case self::PAYMENT_ACTON_PAYMENT:
            case self::PAYMENT_ACTON_PAYWAY_LIST:
            case self::PAYMENT_ACTON_TEST_ECOMMERCE:
                break;

            default:
                $message = sprintf('Requested action "%s" not supported', $action);
                Logger::log(Logger::EMERGENCY, $message);
                throw new RuntimeException($message);
                break;
        }

        return sprintf('https://%s%s', $domain, $action);
    }

    /**
     * Generates hash.
     *
     * @param array $data
     *
     * @return string
     */
    final public static function generateHash(array $data)
    {
        $result = '';
        foreach ($data as $name => $value) {
            if (mb_strtolower($name) === 'hash' || empty($value)) {
                unset($data[$name]);
                continue;
            }
            if (is_array($value)) {
                $value = array_filter($value, 'mb_strlen');
                $value = implode(self::$hashingSeparator, $value);
            }
            if (!empty($value)) {
                $result .= $value . self::$hashingSeparator;
            }
        }
        $result .= self::$hashingSalt;

        return hash(self::$hashingAlgorithm, $result);
    }

    /**
     * @api
     * @return string
     * @throws RuntimeException
     */
    final public function doPaywayList()
    {
        $fields = array(
            'ServiceID' => self::$serviceId,
            'MessageID' => md5(time()),
        );
        $fields['Hash'] = self::generateHash($fields);

        $request = new GuzzleHttp\Psr7\Request('POST', self::getActionUrl(self::PAYMENT_ACTON_PAYWAY_LIST));

        $client = new GuzzleHttp\Client(array(
            GuzzleHttp\RequestOptions::VERIFY => true,
            'exceptions' => false,
        ));

        $responseObject = $client->send($request, array(GuzzleHttp\RequestOptions::FORM_PARAMS => $fields));
        $this->response = (string) $responseObject->getBody();
        $this->isErrorResponse();

        $responseParsed = XMLParser::parse($this->response);

        $model = PaywayList\Transformer::toModel($responseParsed);
        $model->validate((int) $fields['ServiceID'], (string) $fields['MessageID']);

        return $model;
    }
}
